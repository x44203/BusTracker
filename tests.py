import bus, os

datapath = "data"
testBus = bus.Bus(762, 10.214818, 50.051777, 3, 2.3, 1533621716)
testBus2 = bus.Bus(testBus.id, 10.214848, 50.051802, 6, 1.15, 1533621234)
testDelta = bus.Delta(testBus.id, 30, 25, 6, 115, 1533621234)


serdata = testBus.serialize()
unserBus = bus.Bus.deserialize(serdata)
def assertStuff(a, b):
    assert a.id == b.id
    assert a.ns > b.ns - 0.0000001 and a.ns < b.ns + 0.0000001
    assert a.we > b.we - 0.0000001 and a.we < b.we + 0.0000001
    assert a.dir == b.dir
    assert a.speed > b.speed - 0.00001 and a.speed < b.speed + 0.00001
    assert a.timestamp == b.timestamp

assertStuff(testBus, unserBus)
assertStuff(testBus + testDelta, testBus2)


if not os.path.exists(datapath):
    os.makedirs(datapath)
busses = {762: testBus}
bus.saveFile("data/test", busses)

with open(os.path.join(datapath, "test"), "ab") as file:
    file.write(testDelta.serialize())

assertStuff(testBus2, bus.loadFileAndCalculateBusses(os.path.join(datapath, "test"))[762])
