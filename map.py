import folium, bus, os, random, datetime, numpy, copy, configparser

config = configparser.ConfigParser()
config.read("config.cfg")

datapath = config.get("general", "datapath")
mapspath = config.get("map", "mapspath")
tiles = config.get("map", "tiles")
center = [float(comp) for comp in config.get("map", "center").split()]
speedgrid_points = int(config.get("map", "gridpoints"))

if not os.path.exists(mapspath):
    os.makedirs(mapspath)

def getCoordinate(bounds, lat, lon, subdivisions):
    subdivisions -= 1
    minlat = bounds[0][0]
    minlon = bounds[0][1]
    latRange = bounds[1][0] - bounds[0][0]
    lonRange = bounds[1][1] - bounds[0][1]
    return int((lat - minlat) / latRange * subdivisions), int((lon - minlon) / lonRange * subdivisions)

def getColor(speedSum, count):
    return clipColor([
        255 / 90.0 * speedSum / count if count > 0 else 0, # Busse fahren höchstens 90.6 km/h
        0,
        count,
        50 + count if count > 0 else 0 #count + speedSum / count if count > 0 else 0
    ])

def clipColor(col):
    for i in range(len(col)):
        col[i] = min(col[i], 255)
    return col

def generateMap(file):
    initialBusses, deltas = bus.loadFile(os.path.join(datapath, file))

    points = {}
    bounds = [[1000.0, 1000.0], [-1000.0, -1000.0]] # [[lat_min, lon_min], [lat_max, lon_max]]

    busses = copy.deepcopy(initialBusses)

    for id, abus in busses.items():
        points[id] = [[abus.ns, abus.we]]
        bounds[0][0] = min(bounds[0][0], abus.ns)
        bounds[0][1] = min(bounds[0][1], abus.we)
        bounds[1][0] = max(bounds[1][0], abus.ns)
        bounds[1][1] = max(bounds[1][1], abus.we)
    
    dc = 0
    for delta in deltas:
        busses[delta.id] += delta
        points[delta.id].append([busses[delta.id].ns, busses[delta.id].we])
        bounds[0][0] = min(bounds[0][0], busses[delta.id].ns)
        bounds[0][1] = min(bounds[0][1], busses[delta.id].we)
        bounds[1][0] = max(bounds[1][0], busses[delta.id].ns)
        bounds[1][1] = max(bounds[1][1], busses[delta.id].we)
        dc = dc + 1
    
    busses = copy.deepcopy(initialBusses)
    speedgrid = numpy.zeros((speedgrid_points, speedgrid_points, 2)) #[x][y][0] ist Summe und [x][y][1] ist Anzahl

    for id, abus in busses.items():
        x, y = getCoordinate(bounds, abus.ns, abus.we, speedgrid_points)
        speedgrid[speedgrid_points - 1 - x][y][0] += abus.speed
        speedgrid[speedgrid_points - 1 - x][y][1] += 1
    
    for delta in deltas:
        busses[delta.id] += delta
        x, y = getCoordinate(bounds, busses[delta.id].ns, busses[delta.id].we, speedgrid_points)
        speedgrid[speedgrid_points - 1 - x][y][0] += busses[delta.id].speed
        speedgrid[speedgrid_points - 1 - x][y][1] += 1
    
    colorgrid = numpy.zeros((speedgrid_points, speedgrid_points, 4), numpy.int)

    for x in range(speedgrid_points):
        for y in range(speedgrid_points):
            colorgrid[x][y] = getColor(speedgrid[x][y][0], speedgrid[x][y][1])
    
    print(str(dc) + " " + str(bounds))

    m = folium.Map(location=center, tiles=tiles, zoom_start=12)

    for id, line in points.items():
        folium.vector_layers.PolyLine(line, tooptip=str(id), color='#' + str(hex(random.Random(id).randint(0,16777215))).split("x")[1], weight=2).add_to(m)
    
    folium.raster_layers.ImageOverlay(colorgrid, bounds=bounds).add_to(m)

    m.save(os.path.join(mapspath, file[:-4]) + '.html')


for file in os.listdir(datapath):
    if file.endswith(".bus") and (not os.path.exists(os.path.join(mapspath, file[:-4]) + '.html') or file[:-4] == datetime.datetime.now().isoformat().split('T')[0]):
        startTime = datetime.datetime.now()
        generateMap(file)
        print(datetime.datetime.now() - startTime, " s")