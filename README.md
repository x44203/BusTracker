# BusTracker
Bustracker für Schweinfurt ausgelegt für Datensparsamkeit.
## Installation
Benötigt sind Python3, numpy und folium
## Nutzung
Zum Tracken
```bash
    python fetcher.py
```
ausführen.
Es kann mit `Ctrl-C` oder `kill -S INT \<PID\>` beendet werden und neu gestartet werden, es macht beim letzten Punkt weiter.
Und um Karten zu generieren und aktualisieren
```bash
    python map.py
```
## Bilder und Beispiele

![Screenshot vom Terminal](images/fetcher.png)

![Screenshot von einer Karte](images/map.png)