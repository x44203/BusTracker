import bus, os, datetime, configparser

config = configparser.ConfigParser()
config.read("config.cfg")

datapath = config.get("general", "datapath")
url = config.get("fetcher", "url")

#Array: [[Name, Wert],...]
def printMessage(parts):
    first = ""
    second = ""
    for part in parts:
        width = max(len(part[0]), len(part[1]))
        first += part[0].ljust(width) + " | "
        second += part[1].ljust(width) + " | "
    print("\033[F\033[F\033[K" + first[:-3])
    print("\033[K" + second[:-3])


def main():
    print("End with 'kill -s INT %d' or by pressing CTRL + C" % os.getpid())

    if not os.path.exists(datapath):
        os.makedirs(datapath)
    
    deltaCount = 0
    startTime = datetime.datetime.now()
    lastFetch = datetime.datetime.now()
    numBytes = 0
    numFetches = 0
    minDelay = 1E9
    maxDelay = 0

    while True:
        lastOpen = datetime.datetime.now().isoformat().split('T')[0]
        filename = os.path.join(datapath, lastOpen + ".bus")
        print("Writing " + filename)

        busses = {}
        if os.path.exists(filename):
            print("Loading file...")
            busses = bus.loadFileAndCalculateBusses(filename)
        else:
            print("Fetching..")
            busses = bus.getBusses(url)
            numBytes += bus.saveFile(filename, busses)
        
        print("Deltas\t| Delay\t\t| Avg Delay\t| Bytes\t\t| Bytes per day")
        print()
        with open(filename, "ab") as file:
            while lastOpen == datetime.datetime.now().isoformat().split('T')[0]:
                try:
                    numFetches += 1
                    for delta in bus.calculateDeltasFromBusses(busses, bus.getBusses(url)):
                        file.write(delta.serialize())
                        deltaCount += 1
                        numBytes += 14
                except TypeError as e:
                    print(e)
                now = datetime.datetime.now()
                delay = (now - lastFetch).total_seconds()
                minDelay = min(delay, minDelay)
                maxDelay = max(delay, maxDelay)
                printMessage([
                    ["Deltas", str(deltaCount)],
                    ["Delay", f"{delay:.3f} s"],
                    ["Min Delay", f"{minDelay:.3f} s"],
                    ["Avg Delay", f"{(now - startTime).total_seconds() / numFetches:.3f} s"],
                    ["Max Delay", f"{maxDelay:.3f} s"],
                    ["Bytes", str(numBytes) + " B"],
                    ["Bytes per day", str(int(numBytes * 86400.0 / (now - startTime).total_seconds())) + " B/d"]
                ])
                lastFetch = datetime.datetime.now()

main()